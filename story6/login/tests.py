from django.test import TestCase
from django.test import Client
#from .views import index, addition, substraction
from .models import Status
from django.urls import resolve

# Create your tests here.
class loginTest(TestCase):

	def test_request_login(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_template_login(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'profile.html')

	def test_template_landingpage(self):
		response = Client().get('/landingpage/')
		self.assertTemplateUsed(response, 'index.html')

	def test_func_login(self):
		found = resolve('/login/')
		self.assertEqual(found.func, index)

	def test_create_object_model(self):
		new_status = Status.objects.create(mystatus='Saya baik-baik saja')
		counting_object_status = Status.objects.all().count()
		self.assertEqual(counting_object_status, 1)

