from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import addition

# Create your tests here.
class LandingPageTest(TestCase):
	
	def test_landing_page_exists(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	# def test_index_func(self):
	# 	found = resolve('')
	# 	self.assertEqual(found.func, index)

	def test_addition_func(self):
		self.assertEqual(-10, addition(-5,-5))
		self.assertEqual(0, addition(0,0))