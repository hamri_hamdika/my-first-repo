from django.shortcuts import render
import requests

def index(request):
	response = {}
	html = 'index.html'
	return render(request, html, response)

def addition(arg1, arg2):
	return arg1 + arg2 