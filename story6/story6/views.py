from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
	return render(request, 'index.html', response)

def addition(arg1, arg2):
	if (arg1 == 10) : return 0

	return arg1 + arg2

def substraction(arg1, arg2):
	return arg1 - arg2