from django.test import TestCase
from django.test import Client
from .views import index, addition, substraction
from .models import Status
from django.urls import resolve

# Create your tests here.
class story6Test(TestCase):

	def test_request_story6(self):
		response = Client().get('/story6/')
		self.assertEqual(response.status_code, 200)

	def test_template_story6(self):
		response = Client().get('/story6/')
		self.assertTemplateUsed(response, 'index.html')
	
	def test_template_landingpage(self):
		response = Client().get('/story6/')
		self.assertTemplateUsed(response, 'index.html')
	
		
	def test_func_story6(self):
		found = resolve('/story6/')
		self.assertEqual(found.func, index)

	#def test_func_addition(self):
		#self.assertEqual(addition(3,5), 8)
		#self.assertEqual(addition(10,5), 0)

	#def test_func_substraction(self):
		#self.assertEqual(substraction(5,3), 2)

	def test_create_object_model(self):
		new_status = Status.objects.create(mystatus='Saya baik-baik saja')
		counting_object_status = Status.objects.all().count()
		self.assertEqual(counting_object_status, 1)